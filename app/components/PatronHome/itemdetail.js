import React, { Component } from 'react'
import { Text, View,TextInput ,StyleSheet,SectionList,Alert} from 'react-native'
import Api from '../../helpers/api';
export default class itemdetail extends Component {
 
  constructor(props) {
    super(props);
    this.state = {   title: '',
    author: '',
    type:'',
    copies:[],
    copyDetails:[],
    sections:[]
  };
    global.copies = [];
    global.copyDetails = [];

  }

  GetSectionListItem = item => {
    //Function for click on an item
   // Alert.alert(item);
   this.props.navigation.navigate('test');
  };
  FlatListItemSeparator = () => {
    return (
      //Item Separator
      <View
        style={{ height: 0.5, width: '100%', backgroundColor: '#C8C8C8' }}
      />
    );
  };

  componentDidMount() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('index', '');
    console.warn(itemId);
    Api.post('/item/get-item-details.php', new URLSearchParams({
      itemId: itemId
    }).toString()).then(response => {
      // console.log(response);
        //console.warn(response);
        if(response.status=="200"){
           console.warn(response.data.itemTitle);
           this.setState({title: "Title: "+response.data.itemTitle});
           this.setState({author: "Author: "+response.data.itemAuthor});
           this.setState({type: "Type: "+response.data.itemType});
           let copyNumber = 0;
           response.data.copies.forEach(function(item){
          //   console.log( global.items);
          //  items.push({"key":item.itemLbpTitle})     
          copyNumber++;
        //  console.warn(item)   
          // global.copies.push({"title":"Copy "+copyNumber,"data":"item"})     
         // global.copies.push("Copy "+copyNumber) ;
        //   let copy = [];
        //   copy.push({ title: "Copy "+copyNumber, data:  [{id:  copyNumber,value:item.itloName},{id:  copyNumber,value:item.itdeCallNumber}] }) 
        // //  copy.push({id:  copyNumber++,value:item.itdeCallNumber}) 
        //  copy.push({id:  copyNumber++,value:item.ptrnFirstname}) 
          if(item.available==true){
          global.copyDetails.push({ title: "Copy "+copyNumber, data:  [{id:  copyNumber,value:'Status: Available'},{id:  copyNumber,value:'Location: '+item.itloName},{id:  copyNumber,value:'Call No: '+item.itdeCallNumber}] }) ;
          }
          else{
            global.copyDetails.push({ title: "Copy "+copyNumber, data:  [{id:  copyNumber,value:'Status:Not Available'},{id:  copyNumber,value:'Patron: '+item.ptrnFirstname+' '+item.ptrnLastname},{id:  copyNumber,value:'Call No: '+item.itdeCallNumber},{id:  copyNumber,value:'Email: '+item.ptrnEmail}] }) ;

          }
          });
          // this.setState({copies:  global.copies})
          this.setState({sections:  global.copyDetails})
          this.setState({copyDetails:  this.state.copyDetails.concat( global.copyDetails)})

          // console.warn(global.copies[0]) 
          // console.warn(global.copyDetails) 
          // console.warn("nj"+this.state.sections) 
        }
        else{
          // console.warn("Incorrect Credentials");
        }
       })
  }
  render() {
    

    // var A = [
    //   { id: '1', value: 'Afghan\nistan' },
    //   { id: '2', value: 'Afghanistan' },
    //   { id: '3', value: 'Afghanistan' },
    // ];
    // var B = [
    //   { id: '4', value: 'Benin' },
    //   { id: '5', value: 'Bhutan' },
    //   { id: '6', value: 'Bosnia' },
    //   { id: '7', value: 'Botswana' },
    //   { id: '8', value: 'Brazil' },
    //   { id: '9', value: 'Brunei' },
    //   { id: '10', value: 'Bulgaria' },
    // ];
    // var C = [
    //   { id: '11', value: 'Cambodia' },
    //   { id: '12', value: 'Cameroon' },
    //   { id: '13', value: 'Canada' },
    //   { id: '14', value: 'Cabo' },
    // ];


    
    return (
      <View>
      <TextInput
      value = {this.state.title}
      style={styles.input}
      editable = {false}      />
      <TextInput
      value = {this.state.author}
      style={styles.input}
      editable = {false}      />
      <TextInput
      value = {this.state.type}
      style={styles.input}
      editable = {false}      />

      <SectionList
          ItemSeparatorComponent={this.FlatListItemSeparator}
          sections= {global.copyDetails}
          renderSectionHeader={({ section }) => (
            <Text style={styles.SectionHeaderStyle}> {section.title} </Text>
          )}
          renderItem={({ item }) => (
             
            // Single Comes here which will be repeatative for the FlatListItems
            <Text
              style={styles.SectionListItemStyle}
              //Item Separator View
              onPress={this.GetSectionListItem.bind(
                this,
                'Id: ' + item.id + ' Name: ' + item.value
              )}>
              {item.value}
            
            </Text>
          )}
          keyExtractor={(item, index) => index}
        />

      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
   

   
  },
 
  input: {
    width:'80%',
    alignItems: 'center',
    margin: 15,
    height: 30,
    fontSize:14

  },
  SectionHeaderStyle: {
    backgroundColor: '#CDDC89',
    fontSize: 20,
    padding: 5,
    color: '#fff',
  },

  SectionListItemStyle: {
    fontSize: 15,
    padding: 15,
    color: '#000',
    backgroundColor: '#F5F5F5',
  }
});