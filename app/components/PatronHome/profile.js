import React, { Component } from 'react'
import { TextInput,Modal,Alert,Text,Button, View,SectionList,StyleSheet ,Switch} from 'react-native'
import { TabRouter } from 'react-navigation';
//import { TextInput } from 'react-native-gesture-handler';
import Api from '../../helpers/api'

export default class profile extends Component {
    
    constructor(props) {
        super(props);
        this.state = {    isSwitchOn:false,
          ModalVisibleStatus: false ,
          PasswordModalVisibleStatus:false,
          emailInput:"",
          oldPassword:"",
          newPassword:"",
          confirmPassword:""
        }
      };
      componentDidMount(){
        Api.get('/patron/privacy-setting.php', new URLSearchParams({
        }).toString()).then(response => {
        if(response.data.ptrnAnonymous==true){
          this.setState({isSwitchOn:true});
        }
        else{
          this.setState({isSwitchOn:false});

        }
      })
      .catch( err => {
       this.setState({TextInputVisibleStatus:true})
       console.warn('Barcode does not exist');
    })
      }
      ShowModalFunction(visible) {
        console.warn("dd");
            this.setState({ModalVisibleStatus: visible});
            console.warn(this.state.ModalVisibleStatus);
            
          }

          changeEmail(){
            if(this.state.emailInput.includes("@")){
            Api.post('/patron/email-reset.php', new URLSearchParams({
              newEmail: this.state.emailInput
            }).toString()).then(response => {
                if(response.status=="200"){
                   Alert.alert("Email is successfully changed");
                   this.setState({ModalVisibleStatus: false});
                }
                  else{
                    Alert.alert("Email is not changed,Please try again");
                  }
               })
               .catch( err => {
                this.setState({TextInputVisibleStatus:true})
                console.warn('Barcode does not exist');
             })
            }   
            else{
              Alert.alert("Please enter valid email")
            }
          }
        
          changePassword(){
            Api.post('/patron/password-reset.php', new URLSearchParams({
              ptrnOldPassword: this.state.oldPassword,
              ptrnNewPassword: this.state.newPassword,
              ptrnConfirmPassword: this.state.confirmPassword
            }).toString()).then(response => {
                if(response.status=="200"){
                //  Alert.alert("Password is successfully changed");
                   this.setState({PasswordModalVisibleStatus: false});
                   this.setState({oldPassword: ""});
                   this.setState({newPassword: ""});
                   this.setState({confirmPassword: ""});
                   Alert.alert("Password is successfully changed");

                }
               else if(response.status=="409"){
                  Alert.alert("Old and new password should never same");
               }
               else if(response.status=="400"){
                Alert.alert("New and confirm password should be same");
             }
                  else{
                    Alert.alert("Password is not changed,Please try again");
                  }
                
              
               })
               .catch( err => {
               // this.setState({TextInputVisibleStatus:true})
               Alert.alert("Invalid inputs");
              })
          }
        

    FlatListItemSeparator = () => {
        return (
          //Item Separator
          <View
            style={{ height: 0.5, width: '100%', backgroundColor: '#C8C8C8' }}
          />
        );
      };
      GetSectionListItem = item => {
        //Function for click on an item
       //Alert.alert(item.toString());
     // this.setState({isSwitchOn:TabRouter})
     if(item.toString()=="4"){
     this.ShowModalFunction(!this.state.ModalVisibleStatus)
     }
     if(item.toString()=="5"){
      this.setState({PasswordModalVisibleStatus:true})
      }
      if(item.toString()=="6"){
        //this.setState({isSwitchOn:!this.state.isSwitchOn})
        Api.post('/patron/privacy-setting.php', new URLSearchParams({
          isAnonymous:!this.state.isSwitchOn
        }).toString()).then(response => {
        this.componentDidMount();
      })
      .catch( err => {
       console.warn('');
    })
        }
     //  this.props.navigation.navigate('test');
      };
  render() {
      
    return (
  <View >     
       <SectionList
          ItemSeparatorComponent={this.FlatListItemSeparator}
          sections= {[{ title: "Patron", data:  [{id:  1,value:global.ptrnName}]},{ title: "Ids", data:  [{id:  2,value:'Patron id:'+global.ptrnId},{id:  3,value:'Org id:'+global.orgId}]},{ title: "Change Credentials", data:  [{id:  4,value:'View or change Email'},{id:  5,value:'Change Password'}]}]}
          renderSectionHeader={({ section }) => (
            <Text style={styles.SectionHeaderStyle}> {section.title} </Text>
          )}
          renderItem={({ item }) => (
             
            // Single Comes here which will be repeatative for the FlatListItems
            <Text
              style={styles.SectionListItemStyle}
              //Item Separator View
              onPress={this.GetSectionListItem.bind(
                this,
                item.id 
              )}>
              {item.value}
            
            </Text>
          )}
          keyExtractor={(item, index) => index}
        />
        <SectionList
          ItemSeparatorComponent={this.FlatListItemSeparator}
          sections= {[{ title: "Privacy", data:  [{id:  6,value:'Show/Hide checked out items from\nother Patrons'}]}]}
          renderSectionHeader={({ section }) => (
            <Text style={styles.SectionHeaderStyle}> {section.title} </Text>
          )}
          renderItem={({ item }) => (
             
            // Single Comes here which will be repeatative for the FlatListItems
            <View style={{flexDirection:'row', flexWrap:'wrap', backgroundColor: '#F5F5F5'}}>
            <Text
              style={styles.SectionListItemStyle}
              //Item Separator View
            >
              {item.value}
             
            </Text>
            <Switch
          style={{marginTop:17,marginLeft:5}}
          onValueChange = {this.GetSectionListItem.bind(
                this,
                item.id 
              )}
        value={this.state.isSwitchOn}
         />
            </View>
          
          )}
          keyExtractor={(item, index) => index}
        />

        <Modal
          transparent={true}
          animationType={"slide"}
          visible={this.state.ModalVisibleStatus}
          onRequestClose={ () => { this.ShowModalFunction(!this.state.ModalVisibleStatus)} } >
            <View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={styles.ModalInsideView}>
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}

                    <Text style={styles.TextStyle}>Email </Text>
                    <TextInput
                    style={styles.input} 
                    onChangeText={(text) => this.setState({emailInput: text})}
                    placeholder="Enter new email"
                    />
                    <Button  title="Cancel" onPress={() => { this.ShowModalFunction(!this.state.ModalVisibleStatus)} } />
                    <Button  title="Change Email" onPress={() => {this.changeEmail() } } />
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                </View>
            </View>
        </Modal>
        
        <Modal
          transparent={true}
          animationType={"slide"}
          visible={this.state.PasswordModalVisibleStatus}
          onRequestClose={ () => { this.ShowModalFunction(!this.state.PasswordModalVisibleStatus)} } >
            <View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={styles.ModalInsideView}>
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}

                    <Text style={styles.TextStyle}>Change Password </Text>
                    <TextInput
                    style={styles.input}  secureTextEntry={true}
                    onChangeText={(text) => this.setState({oldPassword: text})}
                    placeholder="Old Password"
                    />
                    <TextInput
                    style={styles.input}  secureTextEntry={true}
                    onChangeText={(text) => this.setState({newPassword: text})}
                    placeholder="New Password"
                    />
                    <TextInput
                    style={styles.input}  secureTextEntry={true}
                    onChangeText={(text) => this.setState({confirmPassword: text})}
                    placeholder="Confirm password"
                    />
                    <Button  title="Cancel" onPress={() => { this.setState({PasswordModalVisibleStatus:false})} } />
                    <Button  title="Change Password" onPress={() => {this.changePassword() } } />
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                </View>
            </View>
        </Modal>


           </View>
    )
  }
}
const styles = StyleSheet.create({
    container: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
     
  
     
    },
   
    input: {
      width:'80%',
      alignItems: 'center',
      margin: 15,
      height: 30,
      backgroundColor: '#CDDC89',
      fontSize:14
  
    },
    SectionHeaderStyle: {
      backgroundColor: '#CDDC89',
      fontSize: 22,
      padding: 5,
      color: '#fff',
    },
  
    SectionListItemStyle: {
      fontSize: 18,
      padding: 15,
      color: '#000',
      backgroundColor: '#F5F5F5',
    },
    TextStyle:{
    
      fontSize: 20, 
      color: "grey",
      padding: 0,
      textAlign: 'center'
    
    },
    ModalInsideView:{
    
      justifyContent: 'center',
      alignItems: 'center', 
      backgroundColor : "white", 
      height: 300 ,
      width: '90%',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    
    }
  });