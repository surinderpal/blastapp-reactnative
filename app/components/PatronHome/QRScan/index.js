import React, { Component } from 'react';
import { Button,Alert, Text, View,Modal } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Api from '../../../helpers/api';
import {AsyncStorage} from 'react-native';

class ProductScanRNCamera extends Component {

  constructor(props) {
    super(props);
    this.camera = null;
    this.barcodeCodes = [];
    global.checkoutItemsTitleArray = "";
    global.returnItemsTitleArray = "";
    global.checkoutItemsIdArray = "";
    global.returnItemsIdArray = "";
    this.state = {ScannedBarcode:"",
      TextInputVisibleStatus:false,
      ModalVisibleStatus: false ,
      camera: {
        type: RNCamera.Constants.Type.back,
	flashMode: RNCamera.Constants.FlashMode.auto,
	barcodeFinderVisible: true
      }
      
    };
  }

  ShowModalFunction(visible) {
console.warn("dd");
    this.setState({ModalVisibleStatus: visible});
    console.warn(this.state.ModalVisibleStatus);
    
  }

  checkItemAvailability(){
    //console.warn(this.state.ScannedBarcode);
    Api.post('/transaction/post-scan-check.php', new URLSearchParams({
      itdeId: this.state.ScannedBarcode
    }).toString()).then(response => {
        if(response.status=="200"){
          if( global.isReturnTabAction==true){
            global.returnItemsTitleArray = response.data.itemLbpTitle;
            global.returnItemsIdArray = response.data.itdeBarcode;
            this.props.navigation.state.params.onBack();
            this.props.navigation.goBack();
          }
         else if(response.data.isAvailable==true){
            
          
            

          global.checkoutItemsTitleArray = response.data.itemLbpTitle;
          global.checkoutItemsIdArray = response.data.itdeId;
          this.props.navigation.state.params.onGoBack();
          this.props.navigation.goBack();
            
          }
          else{
            console.warn(response.data);
            this.setState({TextInputVisibleStatus:true})
          }
        }
        else{
         Alert.alert("Sorry, This item is unavailable.") 
        }
       })
       .catch( err => {
        this.setState({TextInputVisibleStatus:true})
        console.warn('Barcode does not exist');
     })
       
  }

  onBarCodeRead(scanResult) {
    // console.warn(scanResult.type);
    // console.warn(scanResult.data);
    if (scanResult.data != null) {
	if (!this.barcodeCodes.includes(scanResult.data)) {
	  this.barcodeCodes.push(scanResult.data);
    this.setState({ScannedBarcode:scanResult.data });
    this.ShowModalFunction(!this.state.ModalVisibleStatus)
	}
    }
    return;
  }

  async takePicture() {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  }

  pendingView() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'lightgreen',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>Waiting</Text>
        
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>

      <Modal
          transparent={true}
          animationType={"slide"}
          visible={this.state.ModalVisibleStatus}
          onRequestClose={ () => { this.ShowModalFunction(!this.state.ModalVisibleStatus)} } >
            <View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={styles.ModalInsideView}>
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                    <Text style={styles.TextStyle}>Scanned Barcode: {this.state.ScannedBarcode} </Text>
                    <Button  title="Close" onPress={() => { this.ShowModalFunction(!this.state.ModalVisibleStatus)} } />
                    <Button  title="Add to Checkout List" onPress={() => {this.checkItemAvailability() } } />
                    <Text style={(this.state.TextInputVisibleStatus==true) ? styles.ShowTextInput : styles.HideTextInput}>This item does not exist.Please scan valid item.</Text>
                    {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                </View>
            </View>
        </Modal>

        <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
            barcodeFinderWidth={280}
            barcodeFinderHeight={220}
            barcodeFinderBorderColor="white"
            barcodeFinderBorderWidth={2}
            defaultTouchToFocus
            flashMode={this.state.camera.flashMode}
            mirrorImage={false}
            onBarCodeRead={this.onBarCodeRead.bind(this)}
            onFocusChanged={() => {}}
            onZoomChanged={() => {}}
            permissionDialogTitle={'Permission to use camera'}
            permissionDialogMessage={'We need your permission to use your camera phone'}
            style={styles.preview}
            type={this.state.camera.type}
        />
        <View style={[styles.overlay, styles.topOverlay]}>
	</View>
	<View style={[styles.overlay, styles.bottomOverlay]}>
         
	</View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  enterBarcodeManualButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40
  },
  scanScreenMessage: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  MainContainer :{
    
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    
    
    },
    
    ModalInsideView:{
    
      justifyContent: 'center',
      alignItems: 'center', 
      backgroundColor : "white", 
      height: 300 ,
      width: '90%',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    
    },
    
    TextStyle:{
    
      fontSize: 20, 
      marginBottom: 20, 
      color: "grey",
      padding: 20,
      textAlign: 'center'
    
    },
    ShowTextInput:{
    
      fontSize: 20, 
      marginBottom: 5, 
      color: "red",
      padding: 10,
      textAlign: 'center'
    
    },
    HideTextInput:{
    
      fontSize: 0, 
      marginBottom: 5, 
      color: "white",
      padding: 10,
      textAlign: 'center'
    
    }
};

export default ProductScanRNCamera;