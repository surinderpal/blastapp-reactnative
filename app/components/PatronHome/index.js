import React from 'react';
import { Text,Alert,RefreshControl, View ,Button,FlatList,StyleSheet,TouchableHighlight,ImageBackground,TouchableOpacity,TextInput} from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import HistoryTab from './Tabs/history';
import Browse from './Tabs/browse';
import Checkout from './Tabs/checkoutTab';
import Icon from "react-native-vector-icons/FontAwesome"



const TabNavigator = createBottomTabNavigator({
  
  Checkout: {
    screen: Checkout,
    navigationOptions: {
      tabBarLabel:"CHECKOUT",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="shopping-cart" size={25}/>
      )
    }
  },
  Browse: {
    screen: Browse,
    navigationOptions: {
      tabBarLabel:"BROWSE",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="search" size={25}/>
      )
    }
  },
  History: {
    screen: HistoryTab,
    navigationOptions: {
      tabBarLabel:"HISTORY",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="history" size={25}/>
      )
    }
  }



});

export default createAppContainer(TabNavigator);
