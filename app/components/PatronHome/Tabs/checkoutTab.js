import React from 'react';
import { Text,Alert,RefreshControl, View ,FlatList,StyleSheet,TouchableHighlight,ImageBackground,TouchableOpacity,TextInput} from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Api from '../../../helpers/api';
import RadioGroup from 'react-native-radio-buttons-group';
import { Button } from 'react-native-elements'
import Icon from "react-native-vector-icons/FontAwesome"

export default class Checkout extends React.Component {
  
    static navigationOptions =({navigation}) => {
       return {
         title: "CHECKOUT", 
         tabBarOptions: { 
         activeTintColor:'white',
         activeBackgroundColor:"#42b6f4",
         inactiveBackgroundColor:"#dee7ed",
         safeAreaInset:{ bottom: 'never'}
         }
       }  
   }
   
   constructor(props) {
     super(props);   
     global.checkoutItems = [];
     global.index =0;
     global.checkoutItemsArray = [];
     this.state = {barcodeInput:"",
     refreshing: false,
     disableCheckoutButton:true,
     disableClearButton:true,
       FlatListItems: []
     };   
   }
   _onRefresh = () => {
     this.setState({refreshing: true});
    
   }
   
   
   componentDidMount () {
     
    // this.setState({FlatListItems:global.checkoutItemsArray});
   }
   componentDidUpdate() {
     
     //this.setState({FlatListItems:global.checkoutItemsArray});
   }
   refreshFunction(){
     global.checkoutItemsArray.push({"key":global.checkoutItemsTitleArray+"\n"+global.checkoutItemsIdArray,"id":global.index})
     global.index++;
     console.warn(global.checkoutItemsArray); 
     this.setState({FlatListItems:global.checkoutItemsArray});
     this.setState({barcodeInput: ""});
     this.setState({disableCheckoutButton: false})
     this.setState({disableClearButton: false})
   }
   
   onPressAddButton(){
     let inputBarcode = this.state.barcodeInput;
     Api.post('/transaction/post-scan-check.php', new URLSearchParams({
       itdeId: this.state.barcodeInput
     }).toString()).then(response => {
         if(response.status=="200"){
           if(response.data.isAvailable==true){
     global.checkoutItemsTitleArray = response.data.itemLbpTitle;
     global.checkoutItemsIdArray = response.data.itdeId;
     this.refreshFunction();
           }
           else{
             console.warn("Sorry, This item is unavailable.") 
           }
         }
        
       
        }) 
        .catch( err => {
          Alert.alert("Barcode does not exist")    })
   }
   deleteItemFromList(){
    global.checkoutItemsArray = [];
    global.checkoutItemsTitleArray = []
    global.checkoutItemsIdArray=[]
    this.setState({FlatListItems:[]});
    this.setState({disableClearButton:true});

  }

    Checkout(){
      const post = () => {
        this.setState({disableClearButton:true});

        global.checkoutItems.push(global.checkoutItemsIdArray);
        console.warn(global.checkoutItems);
     
       Api.post('/transaction/checkout.php', JSON.stringify({
         items: global.checkoutItems
       })).then(response => {
           if(response.status=="200"){
             console.warn(response);
             global.checkoutItemsArray = [];
             global.checkoutItemsTitleArray = []
             global.checkoutItemsIdArray=[]
             this.setState({FlatListItems:[]});
             Alert.alert("Item(s) successfully checked out");
           }
          
         
          }) 
          .catch( err => {
            console.warn('Barcode does not exist');
         })

      }
     this.setState({disableCheckoutButton: true}, post());   //resetting the checkout button to hide
    // this.setState({FlatListItems:[]});      //clearing the flatlist items
     
    }
    flatlistItemClicked = item => {
      Alert.alert(item.toString());
    };
     _onPressQRButton() {
       this.props.navigation.navigate('qrscan',{ onGoBack: () => this.refreshFunction()});
     }
     FlatListItemSeparator = () => {
       return (
         <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
       );
     };
   
     render() {
       return (
         <View style={{ flex: 1, justifyContent: 'center',height:'100%', alignItems: 'center' }}>
         <FlatList
         refreshControl={
             <RefreshControl
               refreshing={this.state.refreshing}
               onRefresh={this._onRefresh}
             />
           }
    
           data={ this.state.FlatListItems }
           extraData={ this.state.FlatListItems }
           ItemSeparatorComponent = {this.FlatListItemSeparator}
           renderItem={({item ,index}) => 
           <TouchableHighlight onPress={this.flatlistItemClicked.bind(this,item.id)}>
           <Text style={styles.item}  > {item.key} </Text>
                           </TouchableHighlight>
           }
        />
        <TouchableOpacity
          
            style={(this.state.disableClearButton == true) ? styles.checkoutButtonDisable : styles.checkoutButtonEnable}
            disabled={this.state.disableClearButton}
            //onPress={ }
            onPress={this.deleteItemFromList.bind(this)}>
            <Text style={{color:'white',fontSize:18}}><Icon name="trash" size={22}/>CLEAR ITEMS</Text>
          </TouchableOpacity>
     
       
         <ImageBackground source={require('./checkout.png')} style={{width: 300, height: 105,fontSize: 40,marginBottom:10}}>
         <TouchableOpacity
         style={{width: 300, height: 100}}
           >
          </TouchableOpacity>
        
     </ImageBackground>
     <Button containerStyle={{ backgroundColor: 'white',marginLeft:'70%',marginTop:0 }}
     onPress={this._onPressQRButton.bind(this)}
      icon={<Icon name="barcode" size={45}/>}
    title=""
  />
     <Text style={{color:'lightblue'}}>OR </Text>
     <TextInput
           style={styles.input}
           value={this.state.barcodeInput}
           onChangeText={(text) => this.setState({barcodeInput: text})}
           placeholder="Enter Barcode"
         />
         <TouchableOpacity
            style={styles.button}
            //onPress={ }
            onPress={this.onPressAddButton.bind(this)}>
            <Text style={{color:'white'}}> ADD</Text>
          </TouchableOpacity>
   
         {/* {!this.checkoutButtonDisable ? <TouchableOpacity></TouchableOpacity> :} */}
          <TouchableOpacity
          
            style={(this.state.disableCheckoutButton == true) ? styles.checkoutButtonDisable : styles.checkoutButtonEnable}
            disabled={this.state.disableCheckoutButton}
            //onPress={ }
            onPress={this.Checkout.bind(this)}>
            <Text style={{color:'white'}}>CHECKOUT</Text>
          </TouchableOpacity>
     
         </View>
       );
     }
   }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "white"
  },
  headerText: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 70,
    width:"100%",
    backgroundColor: "#c9e7ef"
  },
  searchbar: {
    backgroundColor: "white"
  },
  input: {
    width:'80%',
    alignItems: 'center',
    margin: 40,
    marginBottom:0,
    height: 30,
    fontSize:18,
    backgroundColor:'lightgray',
    //borderColor: 'gray',
    //borderWidth: 1,

  },
  button: {
    width:'15%',
    height:25,
    backgroundColor:'#427af4',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0
  },
  checkoutButtonEnable: {
    width:'45%',
    height:25,
    backgroundColor:'green',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  checkoutButtonDisable: {
    width:'0%',
    height:0,
    backgroundColor:'white',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  clearButtonEnable: {
    backgroundColor:'green',
    width:90,
    height:90,
  },
  clearButtonDisable: {
    backgroundColor:'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  valueText: {
    fontSize: 18, 
    marginBottom: 50,
}
 

});