import React from 'react';
import { Text,Alert,RefreshControl, View ,Button,FlatList,StyleSheet,TouchableHighlight,ImageBackground,TouchableOpacity,TextInput} from 'react-native';
import Api from '../../../helpers/api';
import RadioGroup from 'react-native-radio-buttons-group';

export default class History extends React.Component {
    static navigationOptions =({navigation}) => {
      return {
          title: "HISTORY", 
          tabBarOptions: { 
          activeTintColor:'white',
          activeBackgroundColor:"#42b6f4",
          inactiveBackgroundColor:"#dee7ed",
          safeAreaInset:{ bottom: 'never'}
        }
      }  
    }
      constructor(props) {
        super(props);    
        this.state = {
          FlatListItems: [],
           data: [
            {
                label: 'Borrowing',
                value: "borrowing",
                color: 'green',
                size: 16,
            },
            {
                label: '30 Days',
                value: "30days",
                color: 'blue',
                size: 16,
            },
            {
              label: '60 Days',
              value: "60days",
              color: 'orange',
              size: 16,
          },
          {
            label: '60+ Days',
            value: "60+days",
            color: 'red',
            size: 16,
        }]
        };   
      }
  
      componentDidMount() {
        Api.post('/patron/history.php', new URLSearchParams({
          daysOld: "0"
        }).toString()).then(response => {
            if(response.status=="200"){
              var listItems = [];
           //   var i = 0;
              // for (var item in response.data.requestedList) {
              //    console.warn(response.data[item[0]])
          
              // }
              for(var i=0;i<response.data.requestedList.length;i++){
                let month =["January","February","March","April","May","June","July","August","September","October","November","December"]
                var date = response.data.requestedList[i].trdeDueBack.substring(0, 10)
                var d = new Date(date);
                let itemTitle = response.data.requestedList[i].itemLbpTitle;
                if(itemTitle.length>35){
                  itemTitle = itemTitle.substring(0,35);
                  itemTitle = itemTitle+"....";
                }
                listItems.push({key:itemTitle+"\n Due Date:"+response.data.requestedList[i].trdeDueBack.substring(8, 10)+"th "
                +month[d.getMonth()]+" "+response.data.requestedList[i].trdeDueBack.substring(0, 4)})
              }
           this.setState({FlatListItems:listItems})
              
        
            }
            else{
          
            }
           })
      }
      _onRefresh = () => {
        this.setState({refreshing: true});
       
      }
     
  FlatListItemSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
    );
  };
  
  
  
   // update state
   onPress = data =>{
    // this.setState({data})
    let selectedButton = this.state.data.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
        //var current = [{key:"aaaaaaaaa"},{key:"bbbb"},{key:"cc"}];
       
        if(selectedButton=="borrowing"){
         this.componentDidMount();
        }
         if(selectedButton=="30days"){
          Api.post('/patron/history.php', new URLSearchParams({
            daysOld: "30"
          }).toString()).then(response => {
              if(response.status=="200"){
                if(response.data.requestedList.length==0){
                  this.setState({FlatListItems:[{key:"No transaction in last 30 days"}]})
                }
                else{
                var listItems = [];
                for(var i=0;i<response.data.requestedList.length;i++){
                  console.warn(response.data.requestedList[i]) 
                  let month =["January","February","March","April","May","June","July","August","September","October","November","December"]
    
                  var date = response.data.requestedList[i].trdeReturned.substring(0, 10)
                  var d = new Date(date);
                  console.warn(month[d.getMonth()]);
                  let itemTitle = response.data.requestedList[i].itemLbpTitle;
                  if(itemTitle.length>35){
                    itemTitle = itemTitle.substring(0,35);
                    itemTitle = itemTitle+"....";
                  }
                  listItems.push({key:itemTitle+"\n Returned Date:"+response.data.requestedList[i].trdeReturned.substring(8, 10)+"th "
                  +month[d.getMonth()]+" "+response.data.requestedList[i].trdeReturned.substring(0, 4)})
                }
             this.setState({FlatListItems:listItems})
            } 
          
              }
              else{
            
              }
             })
        }
        if(selectedButton=="60days"){
          Api.post('/patron/history.php', new URLSearchParams({
            daysOld: "60"
          }).toString()).then(response => {
              if(response.status=="200"){
                if(response.data.requestedList.length==0){
                  this.setState({FlatListItems:[{key:"No transaction in last 60 days"}]})
                }
                else{
                var listItems = [];
                for(var i=0;i<response.data.requestedList.length;i++){
                  let month =["January","February","March","April","May","June","July","August","September","October","November","December"]
    
                  var date = response.data.requestedList[i].trdeReturned.substring(0, 10)
                  var d = new Date(date);
                  let itemTitle = response.data.requestedList[i].itemLbpTitle;
                  if(itemTitle.length>35){
                    itemTitle = itemTitle.substring(0,35);
                    itemTitle = itemTitle+"....";
                  }
                  listItems.push({key:itemTitle+"\n Returned Date:"+response.data.requestedList[i].trdeReturned.substring(8, 10)+"th "
                  +month[d.getMonth()]+" "+response.data.requestedList[i].trdeReturned.substring(0, 4)})
                }
             this.setState({FlatListItems:listItems})
            } 
          
              }
              else{
            
              }
             })
        }
  
        if(selectedButton=="60+days"){
          Api.post('/patron/history.php', new URLSearchParams({
            daysOld: "61"
          }).toString()).then(response => {
              if(response.status=="200"){
                if(response.data.requestedList.length==0){
                  this.setState({FlatListItems:[{key:"No old transaction"}]})
                }
                else{
                var listItems = [];
                for(var i=0;i<response.data.requestedList.length;i++){
                  let month =["January","February","March","April","May","June","July","August","September","October","November","December"]
    
                  var date = response.data.requestedList[i].trdeReturned.substring(0, 10)
                  var d = new Date(date);
                  let itemTitle = response.data.requestedList[i].itemLbpTitle;
                  if(itemTitle.length>35){
                    itemTitle = itemTitle.substring(0,35);
                    itemTitle = itemTitle+"....";
                  }
                  listItems.push({key:itemTitle+"\n Returned Date:"+response.data.requestedList[i].trdeReturned.substring(8, 10)+"th "
                  +month[d.getMonth()]+" "+response.data.requestedList[i].trdeReturned.substring(0, 4)})
                }
             this.setState({FlatListItems:listItems})
            } 
          
              }
              else{
            
              }
             })
        }
   
   }
  render() {
  
    return (
        <View style={styles.container}>
            {/* <Text style={styles.valueText}>
                Value = {selectedButton}
            </Text> */}
            <RadioGroup radioButtons={this.state.data} onPress={this.onPress}  flexDirection='row' />
            <FlatList
          data={ this.state.FlatListItems }
          ItemSeparatorComponent = {this.FlatListItemSeparator}
          renderItem={({item ,index}) => 
          <TouchableHighlight>
          <Text style={styles.item}  > {item.key} </Text>
                          </TouchableHighlight>
          }
       />
        </View>
    );
  }
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      backgroundColor: "white"
    },
    headerText: {
      fontSize: 20,
      textAlign: "center",
      margin: 10,
      fontWeight: "bold"
    },
    item: {
      padding: 10,
      fontSize: 20,
      height: 70,
    },
    searchbar: {
      backgroundColor: "white"
    },
    input: {
      width:'80%',
      alignItems: 'center',
      margin: 40,
      marginBottom:0,
      height: 30,
      fontSize:18,
      backgroundColor:'lightgray',
      //borderColor: 'gray',
      //borderWidth: 1,
  
    },
    button: {
      width:'20%',
      height:20,
      backgroundColor:'#427af4',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 0
    },
    checkoutButtonEnable: {
      width:'50%',
      height:20,
      backgroundColor:'green',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 5
    },
    checkoutButtonDisable: {
      width:'50%',
      height:20,
      backgroundColor:'white',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 5
    },
    valueText: {
      fontSize: 18, 
      marginBottom: 50,
  }
   
  
  });