import React from 'react';
import { Text,Alert,RefreshControl, View ,FlatList,StyleSheet,TouchableHighlight,ImageBackground,TouchableOpacity,TextInput} from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Api from '../../../helpers/api';
import RadioGroup from 'react-native-radio-buttons-group';
import { Button } from 'react-native-elements'
import Icon from "react-native-vector-icons/FontAwesome"

export default class Return extends React.Component {
  
    static navigationOptions =({navigation}) => {
       return {
         title: "RETURN", 
         tabBarOptions: { 
         activeTintColor:'white',
         activeBackgroundColor:"#42b6f4",
         inactiveBackgroundColor:"#dee7ed",
         safeAreaInset:{ bottom: 'never'}
         }
       }  
   }
   
   constructor(props) {
     super(props);   
     global.isReturnTabAction=false;
     global.returnItems = [];
     global.returnItemsArray = [];
     this.state = {barcodeInput:"",
     refreshing: false,
     disableCheckoutButton:true,
     disableClearButton:true,
       FlatListReturnItems: []
     };   
   }
   _onRefresh = () => {
     this.setState({refreshing: true});
    
   }
   deleteItemFromList(){
    global.returnItemsArray = [];
    global.returnItemsTitleArray = []
    global.returnItemsIdArray=[]
    this.setState({FlatListReturnItems:[]});
    this.setState({disableClearButton:true});

  }
   
   componentDidMount () {
    // this.setState({FlatListItems:global.checkoutItemsArray});
   }
   componentDidUpdate() {
     
     //this.setState({FlatListItems:global.checkoutItemsArray});
   }
   refreshBackFunction(){
     global.returnItemsArray.push({"key":global.returnItemsTitleArray+"\n "+global.returnItemsIdArray})
     console.warn(global.returnItemsArray); 
     this.setState({FlatListReturnItems:global.returnItemsArray});
     this.setState({barcodeInput: ""});
     this.setState({disableCheckoutButton: false})
     this.setState({disableClearButton: false})
   }
   
   onPressAddButton(){
     Api.post('/transaction/post-scan-check.php', new URLSearchParams({
       itdeId: this.state.barcodeInput
     }).toString()).then(response => {
         if(response.status=="200"){           
     global.returnItemsTitleArray = response.data.itemLbpTitle;
     global.returnItemsIdArray = response.data.itdeBarcode;
     this.refreshBackFunction();
           
         
         }
        else{
          Alert.alert("Barcode does not exist")    

        }
       
        }) 
        .catch( err => {
          Alert.alert("Entered Barcode does not exist")    })
   }
   
    Checkout(){
     this.setState({disableCheckoutButton: true})   //resetting the checkout button to hide
     this.setState({FlatListReturnItems:[]});      //clearing the flatlist items
      global.returnItems.push(global.returnItemsIdArray);
      console.warn(global.returnItems);
   
     Api.post('/transaction/return.php', JSON.stringify({
       items: [global.returnItemsIdArray]
     })).then(response => {
         if(response.status=="200"){
           console.warn(response);
           global.returnItemsArray = [];
           global.returnItemsTitleArray = []
           global.returnItemsIdArray=[]
           this.setState({FlatListReturnItems:[]});
           Alert.alert("Item(s) successfully returned");
         }
        
       
        }) 
        .catch( err => {
          console.warn('Barcode does not exist');
       })
    }
   
     _onPressQRButton() {
      global.isReturnTabAction = true;
       this.props.navigation.navigate('qrscan',{ onBack: () => this.refreshBackFunction()});
     }
     FlatListItemSeparator = () => {
       return (
         <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
       );
     };
   
     render() {
       return (
         <View style={{ flex: 1, justifyContent: 'center',height:'100%', alignItems: 'center' }}>
         <FlatList
         refreshControl={
             <RefreshControl
               refreshing={this.state.refreshing}
               onRefresh={this._onRefresh}
             />
           }
    
           data={ this.state.FlatListReturnItems }
           extraData={ this.state.FlatListReturnItems }
           ItemSeparatorComponent = {this.FlatListItemSeparator}
           renderItem={({item ,index}) => 
           <TouchableHighlight >
           <Text style={styles.item}  > {item.key} </Text>
                           </TouchableHighlight>
           }
        />
        <TouchableOpacity
          
          style={(this.state.disableClearButton == true) ? styles.checkoutButtonDisable : styles.checkoutButtonEnable}
          disabled={this.state.disableClearButton}
          //onPress={ }
          onPress={this.deleteItemFromList.bind(this)}>
          <Text style={{color:'white',fontSize:18}}><Icon name="trash" size={22}/>CLEAR ITEMS({global.checkoutItems.length})</Text>
        </TouchableOpacity>
         <ImageBackground source={require('./checkout.png')} style={{width: 300, height: 105,fontSize: 40,marginBottom:10}}>
         <TouchableOpacity
         style={{width: 300, height: 100}}
           >
          </TouchableOpacity>
        
     </ImageBackground>
     <Button containerStyle={{ backgroundColor: 'white',marginLeft:'70%',marginTop:0 }}
     onPress={this._onPressQRButton.bind(this)}
      icon={<Icon name="barcode" size={45}/>}
    title=""
  />
     <Text style={{color:'lightblue'}}>OR </Text>
     <TextInput
           style={styles.input}
           value={this.state.barcodeInput}
           onChangeText={(text) => this.setState({barcodeInput: text})}
           placeholder="Enter Barcode"
         />
         <TouchableOpacity
            style={styles.button}
            //onPress={ }
            onPress={this.onPressAddButton.bind(this)}>
            <Text style={{color:'white'}}>ADD</Text>
          </TouchableOpacity>
   
          <TouchableOpacity
          
            style={(this.state.disableCheckoutButton == true) ? styles.checkoutButtonDisable : styles.checkoutButtonEnable}
            disabled={this.state.disableCheckoutButton}
            //onPress={ }
            onPress={this.Checkout.bind(this)}>
            <Text style={{color:'white'}}>RETURN</Text>
          </TouchableOpacity>
     
         </View>
       );
     }
   }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "white"
  },
  headerText: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 70,
    width:"100%",
    backgroundColor: "#c9e7ef"
  },
  searchbar: {
    backgroundColor: "white"
  },
  input: {
    width:'80%',
    alignItems: 'center',
    margin: 40,
    marginBottom:0,
    height: 30,
    fontSize:18,
    backgroundColor:'lightgray',
    //borderColor: 'gray',
    //borderWidth: 1,

  },
  button: {
    width:'15%',
    height:25,
    backgroundColor:'#427af4',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0
  },
  checkoutButtonEnable: {
    width:'50%',
    height:25,
    backgroundColor:'green',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  checkoutButtonDisable: {
    width:'50%',
    height:20,
    backgroundColor:'white',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  valueText: {
    fontSize: 18, 
    marginBottom: 50,
}
 

});