import React from 'react';
import { Text,Alert,RefreshControl, View ,Button,FlatList,StyleSheet,TouchableHighlight,ImageBackground,TouchableOpacity,TextInput} from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Api from '../../../helpers/api';
import { SearchBar } from 'react-native-elements';
export default class Browse extends React.Component {
    static navigationOptions =({navigation}) => {
      return {
          title: "BROWSE", 
          
          tabBarOptions: { 
            
          activeTintColor:'white',
          activeBackgroundColor:"#42b6f4",
          inactiveBackgroundColor:"#dee7ed",
          safeAreaInset:{ bottom: 'never'}
        }
      }  
  }
    constructor(props) {
      super(props);     
      this.state = {
        FlatListItems: [],
        SearchText:""
      };   
    }
    updateSearch = search => {
      this.setState({SearchText: search });
      this.componentDidMount();
    };
    componentDidMount() {
      Api.post('/item/get-items.php',
          JSON.stringify({
            "orgId": global.orgId,
            "categories" : [],
            "getFavourites": false,
            "getCategories": true,
            "searchText": this.state.SearchText,
            "searchByAuthor": false,
            "ptrnId": global.ptrnId
          }),
          {
            headers: {'Content-Type': 'application/json'}
            
          })
        .then(response => {
            if(response.status=="200"){
             // console.info('Success');
                let items = [];
                console.warn(response)
               global.items.data.items.forEach(function(item){
               // console.log( global.items);
                let itemTitle = item.itemLbpTitle;
                if(itemTitle.length>36){
                  itemTitle = itemTitle.substring(0,36);
                  itemTitle = itemTitle+"....";
                }
               items.push({"key":itemTitle+"\n "+item.availableCopies+"/"+item.totalCopies,"id":item.itemId})             
              });
              this.setState({FlatListItems: items})
            }
            else{
              // console.warn("no respobse");
            }
           }).catch(err => {
             console.warn(err);
           });
    }
    FlatListItemSeparator = () => {
      return (
        <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
      );
    };
  
  
  
    
  render() {
    const  search  = this.state.SearchText;
  return (
     <View style={styles.container}>
     <SearchBar 
          placeholder="Type Here..."
          onChangeText={this.updateSearch}
          inputContainerStyle={styles.searchbar}
          value={search}
        />
       <FlatList
          data={ this.state.FlatListItems }
          ItemSeparatorComponent = {this.FlatListItemSeparator}
          renderItem={({item ,index}) => 
          <TouchableHighlight onPress={() => this.props.navigation.navigate('itemdetails',{'index': item.id})}>
          <Text style={styles.item}  > {item.key} </Text>
                          </TouchableHighlight>
          }
       />
     </View>
  )
  }
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      backgroundColor: "white"
    },
    headerText: {
      fontSize: 20,
      textAlign: "center",
      margin: 10,
      fontWeight: "bold"
    },
    item: {
      padding: 10,
      fontSize: 20,
      height: 70,
    },
    searchbar: {
      backgroundColor: "white"
    },
    input: {
      width:'80%',
      alignItems: 'center',
      margin: 40,
      marginBottom:0,
      height: 30,
      fontSize:18,
      backgroundColor:'lightgray',
      //borderColor: 'gray',
      //borderWidth: 1,
  
    },
    button: {
      width:'20%',
      height:20,
      backgroundColor:'#427af4',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 0
    },
    checkoutButtonEnable: {
      width:'50%',
      height:20,
      backgroundColor:'green',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 5
    },
    checkoutButtonDisable: {
      width:'50%',
      height:20,
      backgroundColor:'white',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 5
    },
    valueText: {
      fontSize: 18, 
      marginBottom: 50,
  }
   
  
  });