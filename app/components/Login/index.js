import React, { Component } from 'react'
import { Text, Alert,View,TextInput,Image,StyleSheet,TouchableOpacity } from 'react-native'
import Api from '../../helpers/api'
import AsyncStorage from '@react-native-community/async-storage';
import { CheckBox } from 'react-native-elements'

export default class index extends Component {
    constructor(props) {
        super(props);
        
        this.getRememberedEmail();
        this.getRememberedOrgId();
        this.state = {   org: '',
        mykey:"",
        email: '',
        password:'',
        rememberMe:false};
        global.token = '';
        global.ptrnId = '';
        global.orgId = '';
        global.ptrnName = '';
      }

      // saveDataToAsyncStorage()
      // {
      //   let ptrnmail = "";
      //   AsyncStorage.setItem('email',ptrnmail);
      // }
      rememberMeStatus = async() => {
        AsyncStorage.getItem('rememberMe', (error, result) => {
          console.warn("reme"+result)
          if(result=="yes"){
          this.setState({rememberMe:true});
          }
          // this.setState({
          //   loginMobileNo: result,
          // });
        });
     }
     getRememberedEmail = async() => {
      AsyncStorage.getItem('email', (error, result) => {
        console.warn("reme"+result)
        this.setState({email:result});
        // this.setState({
        //   loginMobileNo: result,
        // });
        return result;
      });
   }
   getRememberedOrgId = async() => {
    AsyncStorage.getItem('orgid', (error, result) => {
      console.warn("reid"+result)
      this.setState({org:result});
      // this.setState({
      //   loginMobileNo: result,
      // });
      return result;
    });
 }

     changeCheckboxState(){
       this.setState({rememberMe:true})
       console.warn("ltbhg")
     }
     componentWillMount(){
       this.rememberMeStatus();
     }
      componentDidMount(){
       // console.warn(this.getKey())
        Api.get('/verification/verify-token.php', new URLSearchParams({
        }).toString()).then(response => {
          console.warn(response)
          // if(this.state.rememberMe==true){
            if(response.data.ptrnIsAdmin==false){
              this.props.navigation.navigate( "patronhome" )
            }
            else{
              this.props.navigation.navigate( "adminhome" )

            }

          //}
      })
      .catch( err => {
       this.setState({TextInputVisibleStatus:true})
       console.warn('Barcode does not exist');
    })
      }
    
       
        _onPressButton() {
         // let a = this.props.state.email;
          //console.warn(this.state.org);
          // fetch('https://beaconblast.net/services/patron/login-auth.php'
          // ,{method:'POST',body:JSON.stringify({
          //   orgId: 'test',
          //   ptrnIdOrEmail:'sateek.super@gmail.com',
          //   ptrnPassword:'pass123'
          // }),headers:{'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
          // .then(res => {
          //   console.warn(res.status);
          // })
          Api.post('/patron/login-auth.php', new URLSearchParams({
            orgId: this.state.org,
            ptrnIdOrEmail:this.state.email,
            ptrnPassword:this.state.password
          }).toString()).then(response => {
             console.warn(response);
              if(response.status=="200"){
              //   console.warn(response.headers.map['set-cookie']);
              //   let cookie = response.headers.map['set-cookie'];
              // global.token = cookie.slice(13, cookie.length);
              //   console.warn(token);
              global.ptrnId = response.data.ptrnId;
              global.orgId = this.state.org;
              global.ptrnName = response.data.ptrnFirstname+" "+response.data.ptrnLastname;
              


              if(this.state.rememberMe==true){
                AsyncStorage.setItem('rememberMe','yes');
                AsyncStorage.setItem('email',this.state.email);
              AsyncStorage.setItem('orgid',this.state.org);
                this.setState({password:""});
                this.rememberMeStatus();
              }
            //  this.props.navigation.navigate('patronhome');
            this.setState({password:""});
            this.setState({email:""});
            this.setState({org:""});
            if(response.data.ptrnIsAdmin==false){
              this.props.navigation.navigate( "patronhome" )
            }
            else{
              this.props.navigation.navigate( "adminhome" )

            }
              }
              else{
                // console.warn("Incorrect Credentials");
              }
             })
      //      fetch('https://beaconblast.net/api/patron/login-auth.php', {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      //   },
      //   body: new URLSearchParams({
      //     orgId: "sad",
      //     ptrnIdOrEmail:"ankit.darji@beacontree.net",
      //     ptrnPassword:"password"
      //   }).toString()
      // })
      // .then(response => {
      //   console.log(response);
      //     //console.warn(response);
      //     if(response.status=="200"){
      //       console.warn(response.headers.map['set-cookie']);
      //       let cookie = response.headers.map['set-cookie'];
      //     global.token = cookie.slice(13, cookie.length);
      //       console.warn(token);
      //     this.props.navigation.navigate('patronhome');
      //     }
      //     else{
      //       console.warn("Incorrect Credentials");
      //     }
      //    })
      // .then((response) => response.json())
      // .then((responseJson) => {
      //   console.warn(response.status);
      //    console.log(responseJson);
      //    // this.setState({
      //    //    data: responseJson
      //    // })
      // });
      
      
        }
  render() {
    
    return (
      <View style={styles.container}>
      <Image style={styles.image}
          source={require('./beacontree.png')}
        />
      <TextInput
        style={styles.input}
        onChangeText={(text) => this.setState({org: text})}
        placeholder="Organization"
        value={this.state.org}
      />
      <TextInput
      style={styles.input}
      onChangeText={(text) => this.setState({email: text})}
      placeholder="Email"
      value={this.state.email}
      />
      <TextInput
      style={styles.input} secureTextEntry={true}
      onChangeText={(text) => this.setState({password: text})}
      placeholder="Password"
      value={this.state.password}
      />
      <TouchableOpacity
         style={styles.button}
         onPress={this._onPressButton.bind(this)}

       >
         <Text style={{color:'white'}}> LOG IN </Text>
       </TouchableOpacity>
       <CheckBox
       title='Remember me'
        checked={this.state.rememberMe}
        onPress={() => this.setState({rememberMe: !this.state.rememberMe})}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
    container: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
     
  
     
    },
    image: {
      width:'85%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    input: {
      width:'80%',
      alignItems: 'center',
      margin: 15,
      height: 30,
      fontSize:18,
      borderColor: 'gray',
      borderWidth: 1,
  
    },
    button: {
      width:'40%',
      height:30,
      backgroundColor:'#427af4',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 15
    }
  });