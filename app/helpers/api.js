import axios from 'axios';

// axios.defaults.baseURL = 'https://beacontree.net/api/';


// const baseUrl = 'https://beaconblast.net/api';
global.items = "";
let axiosApi = axios.create({
    baseURL: 'https://beaconblast.net/api',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
});

axiosApi.interceptors.request.use(function (config) {
    // Do something before request is sent
    //console.warn(config);
    return config;
  }, function (error) {
    // Do something with request error
    console.error(error);
    return Promise.reject(error);
  });

// Add a response interceptor
axiosApi.interceptors.response.use(function (response) {
    // Do something with response data
    //console.warn(response);
    global.items = response;
    return response;
  }, function (error) {
    // Do something with response error
    console.error(error);
    return Promise.reject(error);
  });

export default axiosApi;
// const apiPostForm = axiosApi;

// // apiPostForm.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

// const apiPostJson = axiosApi;

// // apiPostJson.defaults.headers.post['Content-Type'] = 'application/json';

// export { apiPostForm, apiPostJson }