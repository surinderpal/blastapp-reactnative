/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import Icon from "react-native-vector-icons/Ionicons"
import React, {Component} from 'react';
import {Platform,SafeAreaView, StyleSheet, Text,TouchableOpacity,TextInput, View,Button} from 'react-native';
import Login from './app/components/Login';
 import PatronHome from './app/components/PatronHome';
 import AdminHome from './app/components/PatronHome/adminhome';
 import QRScan from './app/components/PatronHome/QRScan';
 import ItemDetails from './app/components/PatronHome/itemdetail'
 import Test from './app/components/Modals/test'
import { createStackNavigator,DrawerItems, createSwitchNavigator,createAppContainer,createDrawerNavigator } from 'react-navigation';
import Profile from './app/components/PatronHome/profile'
import Api from './app/helpers/api'
import AsyncStorage from '@react-native-community/async-storage';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  constructor(props) {
    super(props);  }
 
  render() {
    return <Appcontainer />

  }
}


const Root = createStackNavigator({

  login: { 

    screen:Login
  },
   patronhome: { 

     screen:PatronHome,
    //  navigationOptions: {
    //    headerTitle:"BeaconTree",
    //   headerStyle: {
    //     backgroundColor: "lightblue"
    //   }
    // }
    
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "BeaconTree",
        headerLeft: <Icon style={{ paddingLeft: 10 }}
          onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
      };
    }

   },
   adminhome: { 

    screen:AdminHome,
   //  navigationOptions: {
   //    headerTitle:"BeaconTree",
   //   headerStyle: {
   //     backgroundColor: "lightblue"
   //   }
   // }
   
   navigationOptions: ({ navigation }) => {
     return {
       headerTitle: "BeaconTree",
       headerLeft: <Icon style={{ paddingLeft: 10 }}
         onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
     };
   }

  },
   qrscan: { 

    screen:QRScan
   },
   itemdetails: { 

    screen:ItemDetails
   },
 
 
});
const Root2 = createStackNavigator({

  profile: { 

    screen:Profile,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Account",
        headerLeft: <Icon style={{ paddingLeft: 10 }}
          onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
      };
    }
  }
  });
const AppDrawNavigator = createDrawerNavigator({
  HOME: { screen: Root },
  ACCOUNT: { screen: Root2 },
 

}, {
  contentComponent:(props) => (
    <View style={{flex:1}}>
        <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
          <DrawerItems {...props} />
          <TouchableOpacity onPress={()=>
            
            Api.get('/patron/logout.php'
    )
         .then(response => {
           if(response.status=="200"){
            {/* AsyncStorage.setItem('rememberMe',"no");
            AsyncStorage.setItem('email',"");
            AsyncStorage.setItem('orgid',""); */}


            props.navigation.navigate("login")

          
               
           }
           else{
              console.warn("error");
           }
          }).catch(err => {
            console.warn(err);

          }) 
          }>
            <Text style={{margin: 16,fontWeight: 'bold',color: "black"}}>Logout</Text>
          </TouchableOpacity>
        </SafeAreaView>
    </View>
  ),
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle'
},
)
const AppSwitchNavigator = createSwitchNavigator(
  {
   // Home: { screen: Login },
    Dashboard: { screen: AppDrawNavigator }
  })
const container = createAppContainer(Root);
//export default container;  
const Appcontainer = createAppContainer(AppSwitchNavigator)